# BinaryTree
Java Implementation of a Binary Tree 

Implements a binary tree and performs recursive scans, inserts, and deletes.
Also added functionality using a stack class to aid in iterative traversals
of the binary tree. This program also illustrates the problem of deleting a
parent node with two children, on how to reassign the pointers to the child
nodes. 
